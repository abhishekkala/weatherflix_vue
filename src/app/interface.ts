// This wil house all the interfaces needed by WeatherFlix

// Data structure for "Current Weather"
export interface ICurrentWeather {
    city: string;
    country: string;
    image: string;
    date: number;
    temperature: number;
    description: string;
}
