import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { ICurrentWeather } from '../interface';

// Defining anonymous types, i.e. children objects
// Each of these objects can be used as their own named interface
// e.g. an array of weather
interface ICurrentWeatherData {
  weather: [{
    description: string;
    icon: string
  }];
  main: {
    temp: number
  };
  sys:  {
    country: string
  };
  dt: number;
  name: string;
}

export interface IWeatherService {
  getCurrentWeather(city: string, country: string): Observable<ICurrentWeather>;
}

// Injectable annotation helps with (1)keeping this service singleton,
// and (2)leverage Angular's provider system to inject this service into other components
// as long as it's added to AppModules's provider array
@Injectable({
  providedIn: 'root'
})

export class WeatherService implements IWeatherService {

  constructor(private httpClient: HttpClient) { }

  // returns an Observable<ICurrentWeatherData> object
  getCurrentWeather(city: string, country: string): Observable<ICurrentWeather> {
    // api url: https://api.openweathermap.org/data/2.5/weather?q=Richmond,us&appid=xxxx
    return this.httpClient.get<ICurrentWeatherData>(
      `${environment.baseUrl}api.openweathermap.org/data/2.5/weather?` +
            `q=${city},${country}&appid=${environment.appId}`
    ).pipe(map(data => this.transformToICurrentWeather(data))
    );
  }

  // function to tranform ICurrentWeatherData to ICurrentWeather
  private transformToICurrentWeather(data: ICurrentWeatherData): ICurrentWeather {
    return {
      city: data.name,
      country: data.sys.country,
      date: data.dt * 1000,
      image: `http://openweathermap.org/img/w/${data.weather[0].icon}.png`,
      temperature: this.convertKelvinToFahrenheit(data.main.temp),
      description: data.weather[0].description
    };
  }

  private convertKelvinToFahrenheit(kelvin: number): number {
    return kelvin * 9 / 5 - 459.67;
  }
}
