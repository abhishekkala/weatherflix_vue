import { Component, OnInit } from '@angular/core';
import { ICurrentWeather } from '../interface';
import { WeatherService } from '../weather/weather.service';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.less']
})
export class CurrentWeatherComponent implements OnInit {
  current: ICurrentWeather;

  constructor(private weatherService: WeatherService) {
  }

  ngOnInit() {
    // subscribe attaches a listener to the emitter
    this.weatherService.getCurrentWeather('Richmond', 'US')
        .subscribe(data => (this.current = data));
  }
}
