# We are using a Docker Multi-Stage build

##################################################################
# First Stage : 
# >> Defines an image containing the compiled version of our app
##################################################################

# Inherit from the node alpine image, based on alpine distribution.
# Name this stage "node" to allow it to be referenced in later stages
FROM node:8.14.0-alpine as node

# define working dir, i.e current path inside the container
WORKDIR /usr/src/app

# Copy package.json and package.lock.json into the image
COPY package*.json ./

# Install all dependencies
RUN npm install

# Copy rest of the project into the image
# >> We do these in separate steps to take advantage of Docker caching,
# >> also known as layer. This way, subsequent builds of the image 
# >> will be faster, in case the package.json did not change
COPY . .

# Run the build script of the angular project
RUN npm run build --prod

##################################################################
# Second Stage : 
# >> Serve the application using NGINX web server
##################################################################

# Inherit from the official nginx alpine image
FROM nginx:1.15.7-alpine

WORKDIR /usr/src/app

# Copy dist-output from the 1st image, "node", into NGINX public folder
COPY --from=node /usr/src/app/dist/weatherflix /usr/share/nginx/html

# Copy the nginx config file (create/confirm that nginx.conf is in app's root)
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
